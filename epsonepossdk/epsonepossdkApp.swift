//
//  epsonepossdkApp.swift
//  epsonepossdk
//
//  Created by Jordi Puigdellívol on 07/11/2020.
//

import SwiftUI

@main
struct epsonepossdkApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
