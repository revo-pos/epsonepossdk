import Foundation

class EpsonDriver {
    
    let printer:Epos2Printer?
    var series:Epos2PrinterSeries = EPOS2_TM_T88
    
    init(){
        printer = Epos2Printer(printerSeries:series.rawValue, lang:EPOS2_MODEL_ANK.rawValue)
    }
    
}
